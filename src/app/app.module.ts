import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { MainComponent } from './components/main/main.component';
import { AuthoristaionComponent } from './components/authorisation/authoristaion/authoristaion.component';
import { LoginComponent } from './components/authorisation/login/login.component';
import { SignupComponent } from './components/authorisation/signup/signup.component';
import { RabbitComponent } from './components/rabbit/rabbit.component';
import { UserService } from './services/user-service/user.service';
import { ProductService } from './services/product-service/product.service';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainComponent,
    AuthoristaionComponent,
    LoginComponent,
    SignupComponent,
    RabbitComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [
    UserService,
    ProductService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
